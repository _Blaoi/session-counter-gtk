# Handmade by blaoi
CC:=g++
TARGET:=main

# Files and directories
DIR_SOURCES:=src
DIRS_SOURCES:=$(shell find $(DIR_SOURCES) -type d -links 2)
DIR_HEADERS:=inc
DIR_OBJECTS:=bin
DIRS_OBJECTS:=$(addprefix $(DIR_OBJECTS)/,$(patsubst $(DIR_SOURCES)/%,%,$(DIRS_SOURCES)))
EXT_SOURCES:=.cpp
EXT_HEADERS:=.hpp
EXT_OBJECTS:=.o
FIL_SOURCES:=$(patsubst $(DIR_SOURCES)/%,%,$(shell find $(DIR_SOURCES) -name "*$(EXT_SOURCES)"))
FIL_HEADERS:=$(patsubst $(DIR_HEADERS)/%,%,$(shell find $(DIR_HEADERS) -name "*$(EXT_HEADERS)"))
FIL_OBJECTS:=$(FIL_SOURCES:$(EXT_SOURCES)=$(EXT_OBJECTS))
FULL_PATH_SOURCES:=$(addprefix $(DIR_SOURCES)/,$(FIL_SOURCES))
FULL_PATH_HEADERS:=$(addprefix $(DIR_HEADERS)/,$(FIL_HEADERS))
FULL_PATH_OBJECTS:=$(addprefix $(DIR_OBJECTS)/,$(FIL_OBJECTS))

# Flags
INCLUDE_FLAGS:=-include $(FULL_PATH_HEADERS)
DEBUG_FLAGS:=-g
OPTIM_FLAGS:=-O2
WARNS_FLAGS:=-W -Wall -Werror
STD_FLAGS:=-std=c++11
PKG_FLAGS:=`pkg-config gtkmm-3.0 --cflags --libs`
CXXFLAGS:=$(DEBUG_FLAGS) $(OPTIM_FLAGS) $(WARNS_FLAGS) $(STD_FLAGS) $(PKG_FLAGS)

# Targets
all:$(DIR_OBJECTS) $(TARGET)

$(DIR_OBJECTS):
	mkdir -p $(DIR_OBJECTS)
	mkdir -p $(DIRS_OBJECTS)

$(TARGET): $(FULL_PATH_OBJECTS)
	$(CC) -o $(TARGET) $(CXXFLAGS) $(FULL_PATH_OBJECTS)

$(DIR_OBJECTS)/%$(EXT_OBJECTS): $(DIR_SOURCES)/%$(EXT_SOURCES)
	$(CC) -c $^ $(CXXFLAGS) -o $@

clean:	
	rm -rf $(DIR_OBJECTS)

mrproper: clean
	rm -rf $(TARGET)

# Phony
.PHONY: all clean mrproper
