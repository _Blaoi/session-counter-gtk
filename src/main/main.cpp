#include "../../inc/main/main.hpp"
#include "../../inc/gui/Window.hpp"
#include <iostream>
#include <gtkmm/main.h>

int main(int argc, char* argv[]) {
    Gtk::Main app(argc, argv);
    Window fenetre;
    Gtk::Main::run(fenetre);
    return 0;

}
