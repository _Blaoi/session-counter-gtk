#include "../../inc/gui/Window.hpp"
#include "../../inc/gui/SouthPane.hpp"
#include "../../inc/gui/NorthPane.hpp"
#include "../../inc/gui/MainFrame.hpp"

Window::Window():
    northPane(new NorthPane()),
    southPane(new SouthPane())
{
    defineWindowProperties();
    mainFrame = new MainFrame(northPane,southPane);
    show_all();
}

Window::~Window(){
    delete southPane;
    delete northPane;
    delete mainFrame;
}

void Window::defineWindowProperties(){
    set_title("Session counter");
    set_default_size(600,400);
    set_resizable(FALSE);
    set_position(Gtk::WIN_POS_CENTER);
    set_icon_from_file("rsc/icon/icone.svg");
}
