#include "../../inc/gui/SouthPane.hpp"

SouthPane::SouthPane():
    buttonAddTimer(new Gtk::Button("Add")),
    buttonRemoveTimer(new Gtk::Button("Remove")),
    buttonSaveTimers(new Gtk::Button("Save")),
    buttonCancel(new Gtk::Button("Cancel")),
    buttonBox(Gtk::manage(new Gtk::ButtonBox(Gtk::ORIENTATION_VERTICAL)))
{
    buttonBox->add(*buttonAddTimer);
    buttonBox->add(*buttonRemoveTimer);
    buttonBox->add(*buttonSaveTimers);
    buttonBox->add(*buttonCancel);
    
    add(*buttonBox);
}

SouthPane::~SouthPane(){
    delete buttonAddTimer;
    delete buttonRemoveTimer;
    delete buttonSaveTimers;
    delete buttonCancel;
    delete buttonBox;
}
