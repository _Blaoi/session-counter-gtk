#ifndef _SOUTHPANE_HPP_
#define _SOUTHPANE_HPP_
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/frame.h>

class SouthPane : public Gtk::Frame{
    private:
        Gtk::Button* buttonAddTimer;
        Gtk::Button* buttonRemoveTimer;
        Gtk::Button* buttonSaveTimers;
        Gtk::Button* buttonCancel;
        Gtk::ButtonBox* buttonBox;
    public:
        SouthPane();
        virtual ~SouthPane();
};
#endif
