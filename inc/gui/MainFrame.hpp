#ifndef __MAINFRAME_HPP_
#define __MAINFRAME_HPP_
#include "../../inc/gui/NorthPane.hpp"
#include "../../inc/gui/SouthPane.hpp"
#include <gtkmm/paned.h>
class MainFrame : public Gtk::Paned{
    private:
        Gtk::ScrolledWindow* northPane;
        Gtk::Frame* southPane;
    public:
        MainFrame(Gtk::ScrolledWindow* n,Gtk::Frame* s);
        ~MainFrame();
};
#endif
