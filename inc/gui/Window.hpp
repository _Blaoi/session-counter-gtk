#ifndef _WINDOW_HPP_
#define _WINDOW_HPP_
#include <gtkmm/frame.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/window.h>
#include "../../inc/gui/MainFrame.hpp"
class Window: public Gtk::Window {
    private:
        Gtk::ScrolledWindow* northPane;
        Gtk::Frame* southPane;
        MainFrame* mainFrame;
    public:
        Window();
        ~Window();
        void defineWindowProperties();
};
#endif
