#ifndef _MAIN_WINDOW_EVENT_HANDLER_HPP_
#define _MAIN_WINDOW_EVENT_HANDLER_HPP_
#include "../inc/Window.hpp"
class MainWindowEventHandler{
    private:
        Window* window;
    public:
        MainWindowEventHandler();
        ~MainWindowEventHandler();
};
#endif
